# Apresentação sobre Docker

Baseada em uma apresentação criada por [Eduardo Mangeli](https://gitlab.com/EduardoMangeli/apresentacao-docker)

Características

1. Foi removida a parte sobre swarms
2. Algumas coisas foram detalhadas para servir a um curso
3. A licença foi alterada para (CC)(BY)(NC)(SA)

